{-# LANGUAGE GeneralizedNewtypeDeriving #-}

import Data.Array.IArray
import Data.Maybe
import Data.List
import Data.Char

import Control.Monad.State

--import Database.HDBC 
--import Database.HDBC.PostgreSQL
import Database.HaskellDB.HDBC.PostgreSQL
import Database.HaskellDB hiding ((!))

import Network
import System.Posix.Process
import System.Posix.Files
import System.IO
import System.Directory
import Control.Concurrent

import Data.Digest.SHA512

data Color = Black | White deriving Eq
type Board = Array (Int, Int) (Maybe Color)
type Form = [(Int, Int)]
data Move = Move Int Int Color | Pass deriving Eq
type Game = [Move]

data Status = Connected | LoggedOn | Playing | Counting deriving (Eq, Ord)

data ServerState = ServerState {
	username :: Maybe String,
	status :: Status
	-- salt
}

newtype Server a = S {
	runS :: StateT ServerState IO a
} deriving (Monad, MonadState ServerState, MonadIO)

evalServer :: Server a -> IO a
evalServer x = evalStateT (runS x) $ ServerState Nothing Connected


protocolVersion = "0"


say :: String -> Server ()
say = liftIO . hPutStrLn stderr


readMove :: Color -> String -> Maybe Move
readMove c a = case words a of
	[x, y] -> Just $ Move (read x) (read y) c
	["pass"] -> Just Pass
	_ -> Nothing

instance Show Color where
	show Black = "B"
	show White = "W"

readColor 'B' = Black
readColor 'W' = White

other Black = White
other White = Black

{- 
 - unlines $ (zipWith (\x y -> "f " ++ show x ++ " = Just '" ++ [y] ++ "'") [0..18] "ABCDEFGHJKLMNOPQRST") ++ ["f _ = Nothing"]
 -
showX :: Int -> String
showX x = 
-}
instance Show Move where
	show (Move x y c) = unwords [show c, show x, show y]
	show Pass = "pass"


main = do
	--installHandler sigPIPE Ignore Nothing
	--stdin <- getContents 
	{-
	dbconn <- postgresqlConnect [
		("host", "localhost"),
		("dbname", "hategod"),
		("sslmode", "disable")] return
	-}
	pid <- getProcessID
	hPutStrLn stderr "hello"
	evalServer $ server
	return ()
	-- TODO clean db
	--commit dbconn
	--disconnect dbconn

server :: Server ()
server = do
	sequence_ $ repeat (interpreter =<< (liftIO getLine))
	--(mapM_ interpreter) =<< (liftIO $ sequence $ repeat getLine)
	liftIO . maybe (return ()) (removeFile . ("hategod-player." ++)) . username =<< get

interpreter :: String -> Server ()
interpreter x = do
	s <- get
	case words x of
		("username":username:[]) -> if not (isGoodUsername username) then
		        say "error: bad username" else do
		-- TODO auth
			put $ s { username = Just username, status = LoggedOn }
			say $ "user " ++ username
			say "logged on"
		("version":[]) -> say protocolVersion
		("who":[]) -> liftIO who
		("play":username:x:y:[]) -> if status s /= LoggedOn then 
			say "error: wrong status" else if not $ isGoodUsername username then
			say "error: bad username" else do
			put $ s { status = Playing }
			say "playing"
			startGame username (read x) (read y)
		("listen":[]) -> if status s /= LoggedOn then
			say "error: wrong status" else do
			put $ s { status = Playing }
			acceptGame
		_ -> say "error: parse failed"

isGoodUsername = and . map isAlphaNum

who :: IO ()
who = sequence_ . map (hPutStrLn stderr) . catMaybes . map (stripPrefix "hategod-player.") =<< getDirectoryContents =<< getCurrentDirectory

acceptGame :: Server ()
acceptGame = do
	s <- get
	let fn = "hategod-player." ++ (fromJust $ username s)
        (rival, _, _) <- liftIO $ accept =<< (listenOn $ UnixSocket fn)
	x <- liftIO $ hGetLine rival
	y <- liftIO $ hGetLine rival
	g <- play rival (emptyBoard (read x) (read y)) False
	liftIO $ seq g (hClose rival)

startGame :: String -> Int -> Int -> Server ()
startGame user x y = do
	-- connect
	rival <- liftIO $ connectTo "" $ UnixSocket $ "hategod-player." ++ user
	g <- play rival (emptyBoard x y) True
	liftIO $ seq g (hClose rival)
--	countScore g
--	pushToDB
	return ()

play :: Handle -> Board -> Bool -> Server Game
play r b amifirst = let mc = if amifirst then Black else White
			tc = other mc
			mm = myMove r b mc
			tm = getTheirsMove r in
	sequence $ (if amifirst then [mm] else []) ++ cycle [tm, mm]


{-
pushToDB :: Game -> Server ()
pushToDB = return ()	-- TODO
-}

myMove :: Handle -> Board -> Color -> Server Move
myMove r b c = do
	say "iwannayourmove"
	m <- (return . head . filter (isGood b) . repeat) =<< (getMyMove c)
	liftIO $ hPutStrLn r $ show m
	return m


getMyMove :: Color -> Server Move
getMyMove c = liftIO $ (return . fromJust . readMove c) =<< getLine

-- theirs move is always a good one
getTheirsMove :: Handle -> Server Move
getTheirsMove rival = do
	say "igetthm"
	c <- liftIO $ (return . readColor) =<< hGetChar rival
	m <- liftIO $ (return . fromJust . readMove c) =<< getLine
	say $ show m
	return m

gameToBoard :: Game -> Board -> Maybe Board
gameToBoard g b = foldM doMove b g

doMove :: Board -> Move -> Maybe Board
doMove b Pass = Just b
doMove b m@(Move x y c) | isGood b m = Just $ flip doKill c $ doMove_ b m
			| otherwise = Nothing 

doMove_ :: Board -> Move -> Board
doMove_ b (Move x y c) = b // [((x, y), Just c)]

isGood :: Board -> Move -> Bool
isGood b m = and $ (\a b c -> zipWith uncurry a (repeat (b, c))) goodSigns b m
-- TODO

goodSigns :: [Board -> Move -> Bool]
goodSigns = [(\z (Move x y _) -> not $ isOccupied z (x, y)),
	     curry $ not . uncurry isSuicide]

isOccupied :: Board -> (Int, Int) -> Bool
isOccupied b i = maybe False (const True) $ b ! i

isSuicide b m@(Move x y c) = if isKilling b m then False else countDameF b (fromJust $ findForm (doMove_ b m) (x, y)) == 0
	

isKilling b m@(Move x y c) = b /= (flip doKill (other c) $ doMove_ b m)

doKill :: Board -> Color -> Board
doKill board color = remove board $ concat $ filter (\f -> countDameF board f == 0) $ nub $ mapMaybe (\i -> findForm board i) $ indices board

countDameF :: Board -> Form -> Int -- STUPID!
countDameF b f = sum $ map (countDame b) f

countDame :: Board -> (Int, Int) -> Int
countDame b i = sum $ map (b2i . not . isOccupied b) $ findNeighbours b i

remove :: Board -> Form -> Board
remove b f = b // (map (\i -> (i, Nothing))) f

b2i True = 1
b2i False = 0

findForm :: Board -> (Int, Int) -> Maybe Form
findForm b i = liftM (findForm_ b [i]) (b ! i)

findForm_ :: Board -> Form -> Color -> Form
findForm_ b i c = let f = nub $ sort $ concatMap (flip (friendlyNeighbours b) c) i in
	if i == f then f else findForm_ b f c

friendlyNeighbours :: Board -> (Int, Int) -> Color -> [(Int, Int)]
friendlyNeighbours b i c = map fst $ filter (\x -> Just c == snd x) $ map (\i -> (i, b ! i)) $ findNeighbours b i

findNeighbours :: Board -> (Int, Int) -> [(Int, Int)]
findNeighbours b (x, y) = filter (inRange $ bounds b) [(x + 1, y + 1), (x - 1, y + 1), (x + 1, y - 1), (x - 1, y - 1)]

emptyBoard :: Int -> Int -> Board
emptyBoard x y = array ((1, 1), (x, y)) [((a, b), Nothing) | a <- [1..x], b <- [1..y]]

